<?php
/**
 * @author   	Copyright (C) 2019 cg@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>
		<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/montserrat-regular-webfont.ttf">
		<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/montserrat-bold-webfont.ttf">
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/normalize.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/responsive.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/styles.css"; ?>" rel="stylesheet" type="text/css" />		
</head>
<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass . $deskTab; ?>">

	<?php if($this->countModules('modal-start') && ($detectAgent !="phone ") ) : ?>
		<jdoc:include type="modules" name="modal-start" style="none" />
	<?php endif;?>
	<!-- Body -->
		<div id="wrapper" class="fullwidth site_wrapper">
			<div class="above-the-fold">	
			<?php
				if($detectAgent !="phone ") :				
				// including menu
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				elseif ($detectAgent == "phone " && $frontpage ) :
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				else :
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu-phone.php');
				endif;
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');

				if($detectAgent == "phone " && !$frontpage) :						
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu-phone-pos.php');
				endif;
				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');

				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			?>
			</div>
			<div class="below-the-fold">
			<?php 
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');				
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
			?>
			</div>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript">

		jQuery(document).ready(function() {
			
			<?php if($clientMobile) : ?>
				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
				});
			<?php else: //sticky: ?>
				jQuery(window).scroll(function(){
					(jQuery(this).scrollTop() > 90) ? jQuery('.navbar-wrapper').addClass('sticky') : jQuery('.navbar-wrapper').removeClass('sticky');
				});
			<?php endif; ?>	

			<?php //CG: Modal-Start ?>
		    	jQuery('#closeModal').click('on', function(){
		    		jQuery('.modal-start').remove();
		        	sessionStorage.setItem('modal-start', true); 
		    	});
			    if(sessionStorage.getItem('modal-start') != null) { 
			    	jQuery('body').addClass('modalClicked');
			    }

			<?php // scrollToTop: ?>
				jQuery(function(){	 
					jQuery(document).on( 'scroll', function(){
				 
						if (jQuery(window).scrollTop() > 200) {
							jQuery('.scroll-top-wrapper').addClass('show');
						} else {
							jQuery('.scroll-top-wrapper').removeClass('show');
						}
					});	 
					jQuery('.scroll-top-wrapper').on('click', scrollToTop);
				});	 
				function scrollToTop() {
					verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
					element = jQuery('body');
					offset = element.offset();
					offsetTop = offset.top;
					jQuery('html, body').animate({scrollTop: offsetTop}, 650, 'swing');
				}
	});

	<?php //CG: scroll Ani ?>
	    let offsetMinusValue = 350;
		jQuery(window).scroll(function() {
		    var windowBottom = jQuery(this).scrollTop() + jQuery(this).innerHeight();

		    jQuery(".startFade").each(function() {
		    	<?php if($detectAgent == "phone ") : ?>
		    		offsetMinusValue = 1680;
		    	<?php else: ?>
		    		(jQuery(this).outerHeight() > 600) ? offsetMinusValue = 650 : offsetMinusValue = 300; <?php // CG: ein ein Element höher wie "600" dann offsetMinusValue höher damit es nicht so viel später faded wie niedrigere Elemente ?>
		    	<?php endif; ?>
		        var objectBottom = (jQuery(this).offset().top + jQuery(this).outerHeight() - offsetMinusValue );
		        if (objectBottom < windowBottom) { 
		            if (jQuery(this).css("opacity") == 0) {
		            	jQuery(this).fadeTo(800,1); 
		            	jQuery(this).addClass('add-Ani');
		            }
		        }
		        if(jQuery(window).scrollTop() == 0 ) {
		        	jQuery(this).css("opacity", "0");
		        	jQuery(this).removeClass('add-Ani');	
		        } 
		    });
		}).scroll();		

		setTimeout(function(){ <?php //CG: wg Scroll Ani: in mitten der page reload erfordert scroll to Top ?>
			jQuery(window).scrollTop(0);
		}, 700);		
		
	<?php //CG: parent-Menues nicht klickbar machen im breadcrumb ?>
		if(jQuery('body').hasClass('team') || jQuery('body').hasClass('laden')) {
			jQuery('.breadcrumb li:nth-child(3) .pathway').on('click', function(e){ 
						e.preventDefault();
			}); 		
		}
		jQuery('li:not(.item-112).parent>a').on('click', function(event){
				event.preventDefault();
				return false;
		});	

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>
	<div class="scroll-top-wrapper ">
		<span class="scroll-top-inner">
			<span aria-hidden="true">&#8593;</span>
		</span>
	</div>		
</body>
</html>
