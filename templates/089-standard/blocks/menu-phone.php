<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 

?>
<nav class="navbar-wrapper flex fullwidth">
    <div class="vcard">
        <p class="photo">
        <a class="brand" href="<?php echo $this->baseurl; ?>">
          <?php echo $logo; ?>
            <?php if ($this->params->get('sitedescription')) : ?>
              <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
            <?php endif; ?>
          </a>
        </p>
        <p class="org">Schlüsseldienst u. Aufsperrdienst <span>München</span></p>
        <p class="fn">Markus Pichelmaier</p>
        <p>Meisterbetrieb</p>
        <p class="adr"><span class="street-address">Eugen-Papst-Str. 2</span><br />
          <span class="postal-code">82110 </span><span class="region">Germering</span>
        <p class="tel "><a class="" href="tel:+4989845100">Tel: 089 - 84 51 00</a>
        </p>
    </div>
</nav>