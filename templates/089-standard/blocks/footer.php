<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2019 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer">
	<div class="footer-top startFade alt-color">
		<h2 class="uppercase null1-14">Wir freuen uns auf Sie!</h2>
		<div class="logo--small__footer">
			<a class="brand" href="<?php echo $this->baseurl; ?>">
				<img src="/images/schluesseldienst-muenchen-pichelmaier-logo-2.png" alt="Schlüsseldienst München - Markus Pichelmaier Logo" />
			</a>
		</div>
		<div class="vcard"> 
	        <p class="org">Schlüsseldienst u. Aufsperrdienst <span>München</span></p>
	        <p class="fn">Inh. Markus Pichelmaier</p>
	        <p class="adr"><span class="street-address">Eugen-Papst-Str. 2</span><br />
	        <p class="postal-code">82110 </span><span class="region">Germering</p>
	        <br />
	        <p class="tel "><a class="" href="tel:+4989845100">Tel: +49 - (0) 89 - 84 51 00</a></p>
		</div>	
		<div id="copyright" class="fullwidth">
			<div class="copyWrapper">
				<p><a class="imprLink" href="/impressum.html" title="Impressum FIRMA">Impressum</a> | <a class="imprLink" href="/datenschutz.html">Datenschutz</a></p>				
			</div>
		</div>	
	</div>
	<div class="footer--bottom">		
		<?php if ($this->countModules('footnav')) : ?>
			<div class="module_footer position_footnav">
				<jdoc:include type="modules" name="footnav" style="custom" />
			</div>				
		<?php endif ?>
		<p class="copy-paragraph center">&copy; <?php print date("Y") . " " . $sitename; ?></p>		
   	</div>	
</footer>
		